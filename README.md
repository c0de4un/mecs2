# mecs2

Mobile Entity Component System v2
Library for ECS

## Features
 - lightweight;
 - independent from platform-specific APIs;
 - thread-safe;
 - delayed & direct events delivery;
 - optional systems storage to avoid using global-scope pointers;
 - DoD-based structs with alignemnt-support;
 - flexible smart-pointers, allocation API etc with config headers;

## Installation
CMake install support

## Usage
Library build-script supports both SHARED and STATIC types.
Link with it and include public/includes headers.

## Contributing
If you find this library useful, contact me

## Authors and acknowledgment
code4un

## License
MIT

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

